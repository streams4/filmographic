# filmographic
A VueCli app that allows you to consult filmographies in graphical form. A pretext project to test the D3 library and The Movie Database API. Realized in stream on Twitch : https://www.twitch.tv/ostaladafab 

Thanks to Codrops for the tuto : https://github.com/vuejs/core/blob/main/CHANGELOG.md

Version 1.0.0


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
