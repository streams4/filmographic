import { createApp } from "vue";
import axios from "axios";
import App from "./App";
import router from "./router";
import VueAxios from "vue-axios";

createApp(App).use(router).mount("#app");
App.use(axios, VueAxios);
