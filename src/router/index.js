import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const routes = [
    {
        path: "/",
        name: "home",
        component: HomeView,
    },
    {
        path: "/search/:query/:page?",
        name: "search",
        component: () => import("../views/SearchView.vue"),
    },
    {
        path: "/person/:id",
        name: "person",
        component: () => import("../views/PersonView.vue"),
    },
    {
        path: "/404",
        name: "PageNotExist",
        component: () => import("../views/404View.vue"),
    },
    {
        path: "/:catchAll(.*)", // Unrecognized path automatically matches 404
        redirect: "/404",
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
